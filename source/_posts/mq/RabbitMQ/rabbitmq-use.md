---
title: RabbitMQ使用
date: 2018-09-23 23:00:50
tags:
    - MQ
    - 消息队列
---

### RabbitMQ安装

#### 安装erlang
```bash
$ sudo yum install epel-release

$ sudo wget https://packages.erlang-solutions.com/erlang-solutions-1.0-1.noarch.rpm
$ rpm -Uvh erlang-solutions-1.0-1.noarch.rpm
$ sudo yum install erlang
```

#### 安装RabbitMQ
```bash
$ sudo wget https://dl.bintray.com/rabbitmq/all/rabbitmq-server/3.7.8/rabbitmq-server-3.7.8-1.el7.noarch.rpm
$ yum install rabbitmq-server-3.7.8-1.el7.noarch.rpm
```

#### 添加守护进程
```bash
$ chkconfig rabbitmq-server on
```

#### 启动服务
```bash
$ service rabbitmq-server start
```

#### 停止服务
```bash
$ service rabbitmq-server stop
```

#### 开放5672和15672端口
```bash
$ firewall-cmd --zone=public --add-port=5672/tcp --permanent
$ firewall-cmd --zone=public --add-port=15672/tcp --permanent
$ firewall-cmd --reload
```

### 配置
#### 管理RabbitMQ Web管理后台
```bash
$ rabbitmq-plugins enable rabbitmq_management
```

#### 添加用户并授权
```bash
$ rabbitmqctl add_user admin 123456
$ rabbitmqctl set_user_tags admin administrator
$ rabbitmqctl set_permissions -p "/" admin ".*" ".*" ".*"
```
