---
title: git使用笔记
date: 2018-06-29 13:53:43
categories: git usage
tags:
  - git
---

### 撤销

#### commit之后， 未push远程

```git
git reset --soft HEAD^
```

--mixed 
意思是：不删除工作空间改动代码，撤销commit，并且撤销git add . 操作
这个为默认参数,git reset --mixed HEAD^ 和 git reset HEAD^ 效果是一样的。


--soft  
不删除工作空间改动代码，撤销commit，不撤销git add . 

--hard
删除工作空间改动代码，撤销commit，撤销git add . 

注意完成这个操作后，就恢复到了上一次的commit状态。

git commit --amend

此时会进入默认vim编辑器，修改注释完毕后保存就好了。