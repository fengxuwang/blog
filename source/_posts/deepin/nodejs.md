---
title: linux下Node.js环境配置
date: 2018-05-28 13:13:21
categories: 
    - Node.js环境配置
tags:
    - Node.js环境配置
---

### 1.使用tar.xz安装Node.js

#### 1.1 解压到用户目录，如果需要解压到系统目录则配置相应的权限
```bash
$ tar -xvf node-v10.2.1-linux-x64.tar.xz -C ~/software
```

#### 1.2 配置环境变量，编辑.bashrc

```bash
$ vi .bashrc
在文件结尾添加如下配置：
export NODE_HOME=/home/fengxuwang/software/node-v10.2.1-linux-x64
export PATH=$NODE_HOME/bin:$PATH
```