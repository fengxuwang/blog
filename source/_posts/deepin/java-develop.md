---
title: Linux下Java开发环境配置
date: 2018-05-28 18:37:10
categories: 
    - java开发环境配置
tags:
    - java开发环境配置
---

### 安装jdk

#### 下载安装包
```bash
$ wget http://download.oracle.com/otn-pub/java/jdk/8u171-b11/512cd62ec5174c3487ac17c61aaa89e8/jdk-8u171-linux-x64.tar.gz
```

#### 创建安装目录并解压
```bash
$ sudo mkdir -p /usr/lib/jvm
$ sudo tar -zxvf jdk-8u171-linux-x64.tar.gz -C /usr/lib/jvm
```

#### 配置环境变量
```bash
进入用户主目录
$ cd ~
编辑配置文件，修改环境变量
$ vi .bashrc
在文件结尾添加如下内容:
export JAVA_HOME=/usr/local/jvm/jdk1.8.0_171
export JRE_HOME=$JAVA_HOME/jre
export PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$PATH
重新加载配置
$ source .bashrc
```

<!--more-->

### 安装maven

#### 下载安装包
```bash
$ wget http://mirrors.hust.edu.cn/apache/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.tar.gz
```

#### 解压
```bash
$ tar -zxvf apache-maven-3.5.3-bin.tar.gz -C ~/software
```

#### 配置环境变量
```bash
进入用户主目录
$ cd ~
编辑配置文件，修改环境变量
$ vi .bashrc
在文件结尾添加如下内容:
export MAVEN_HOME=/home/fengxuwang/software/apache-maven-3.5.3
export PATH=$MAVEN_HOME/bin:$PATH
重新加载配置文件
$ source .bashrc
```

### 安装Gradle

#### 下载
```
$ wget http://services.gradle.org/distributions/gradle-4.7-bin.zip
```

#### 解压
```
$ unzip gradle-4.7-bin.zip -d ~/software
```

#### 配置环境变量
```bash
进入用户主目录
$ cd ~
编辑配置文件，修改环境变量
$ vi .bashrc
在文件结尾添加如下内容：
export GRADLE_HOME=/home/fengxuwang/software/gradle-4.7
export PATH=$GRADLE_HOME/bin:$PATH
重新加载配置文件
$ source .bashrc
```

### 安装Subversion

#### deepin linux下安装，类Debian都可以使用
```
$ sudo apt-get install subversion
```

### 安装Git

#### deepin linux下安装，类Debian都可以使用
```
$ sudo apt-get install git
```

### 安装Redis

#### 下载
```bash
$ wget http://download.redis.io/releases/redis-4.0.9.tar.gz
```

#### 解压
```bash
$ tar -zxvf redis-4.0.9.tar.gz -C ~/software
```

#### 编译
```bash
进入Redis解压目录
$ cd ~/software/redis-4.0.9
编译
$ make
```