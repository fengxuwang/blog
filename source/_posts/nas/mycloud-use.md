---
title: mycloud使用
date: 2018-07-16 20:06:55
categories: mycloud
tags:
    - nas
---

### 中文乱码

常看字符集
```bash
# locale -e
```

安装vim
```bash
# sudo apt-get update
# sudo apt-get install vim
```

设置字符集
```bash
# cd ~
# vim .bashrc

将
export LC_ALL=C
修改成
export LC_ALL='zh_CN.utf8'
```