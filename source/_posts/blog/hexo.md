---
title: 个人博客-Hexo使用
date: 2018-04-28 03:13:38
tags: 个人博客
categories: 
  - 个人博客
comments: true
---
#### Hexo介绍
>Hexo 是一个快速、简洁且高效的博客框架。

#### 安装 Hexo
使用 npm 安装 Hexo ，请先安装 Node.js
``` bash
$ npm install -g hexo-cli
```
<!--more-->

#### 建站
``` bash
$ hexo init my-blog
$ cd my-blog
$ npm install
```

文件夹的目录如下：
```
.
├── scaffolds       模板
├── source          资源
|   ├── _drafts
|   └── _posts
└── themes          主题
├── _config.yml     站点配置信息
├── package.json    应用程序的信息
```