---
title: mysql使用
date: 2018-08-15 13:38:09
tags:
---

### 基本使用

#### 创建database，设置字符集
```mysql
> create database if not exists test default charset utf8 collate utf8_general_ci;
```

#### 创建用户并授权
```mysql
> create user fengxuwang identified by '123456';

> grant all privileges on `test`.* to 'fengxuwang'@'%';

> flush privileges;
```
