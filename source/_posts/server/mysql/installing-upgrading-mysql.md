---
title: mysql安装和更新
date: 2018-08-15 10:24:20
tags:
---

### 在Linux上使用MySQL APT存储库安装MySQL
1、下载MySQL APT repository
MySQL APT存储库的下载页面，http://dev.mysql.com/downloads/repo/apt/
```
$ sudo wget https://dev.mysql.com/get/mysql-apt-config_0.8.10-1_all.deb

$ sudo dpkg -i mysql-apt-config_0.8.10-1_all.deb

$ sudo apt-get update

$ sudo apt-get install mysql-server
```
