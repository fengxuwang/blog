---
title: spring-cloud
date: 2019-03-05 13:31:47
tags:
---
# Spring Cloud 学习笔记

## Spring Cloud学习项目

### Gradle初始化项目
```bash
$ mkdir spring-cloud-learning
```

gradle初始化项目
```bash
$ cd spring-cloud-learning && gradle init
```

修改spring-cloud-learning目录下的build.gradle
```gradle
plugins {
    id 'org.springframework.boot' version '2.1.3.RELEASE'
}

allprojects {
    group = "org.azaleaframework"
    version '0.0.1-SNAPSHOT'

    apply plugin: 'java'
    apply plugin: 'idea'
}

subprojects {
    apply plugin: 'org.springframework.boot'
    apply plugin: 'io.spring.dependency-management'

    sourceCompatibility = '1.8'
    targetCompatibility =  '1.8'

    repositories {
        maven {url 'http://maven.aliyun.com/nexus/content/groups/public/'}
        mavenCentral()
    }

    dependencyManagement {
        imports {
            mavenBom 'org.springframework.cloud:spring-cloud-dependencies:Greenwich.RELEASE'
        }
    }
}
```

### eureka服务注册中心
在spring-cloud-learning目录下创建eureka-server子模块
```bash
$ mkdir -p eureka-server/src/{main,test}/{java,resources}
```

在spring-cloud-learning目录下的settings.gradle中引入eureka-server子模块
```gradle
include 'eureka-server'
```

在eureka-server添加build.gradle配置文件
```bash
touch eureka-server/build.gradle
```

修改eureka-server目录下的build.gradle配置文件
```gradle
archivesBaseName = 'eureka-server'

dependencies {
    compile('org.springframework.cloud:spring-cloud-starter-netflix-eureka-server')
    testCompile('org.springframework.boot:spring-boot-starter-test')
}
```

### eureka服务提供者
在spring-cloud-learning目录下创建eureka-service-provider子模块
```bash
$ mkdir -p eureka-service-provider/src/{main,test}/{java,resources}
```

在spring-cloud-learning目录下的settings.gradle中引入eureka-service-provider子模块
```gradle
include 'eureka-service-provider'
```

在eureka-service-provider添加build.gradle配置文件
```bash
touch eureka-service-provider/build.gradle
```

修改eureka-service-provider目录下的build.gradle配置文件
```gradle
archivesBaseName = "eureka-service-provider"

dependencies {
    compile('org.springframework.cloud:spring-cloud-starter-netflix-eureka-client')
    compile('org.springframework.boot:spring-boot-starter-web')
    testCompile('org.springframework.boot:spring-boot-starter-test')
}
```

### eureka服务消费者
在spring-cloud-learning目录下创建eureka-service-consumer子模块
```bash
$ mkdir -p eureka-service-consumer/src/{main,test}/{java,resources}
```

在spring-cloud-learning目录下的settings.gradle中引入eureka-service-consumer子模块
```gradle
include 'eureka-service-consumer'
```

在eureka-service-consumer添加build.gradle配置文件
```bash
touch eureka-service-consumer/build.gradle
```

修改eureka-service-consumer目录下的build.gradle配置文件
```gradle
archivesBaseName = "eureka-service-consumer"

dependencies {
    compile('org.springframework.cloud:spring-cloud-starter-netflix-eureka-client')
    compile('org.springframework.boot:spring-boot-starter-web')
    testCompile('org.springframework.boot:spring-boot-starter-test')
}
```

### Config 配置文件
在spring-cloud-learning目录下创建config-repo子模块
```bash
$ mkdir config-repo
```

### Config服务端
在spring-cloud-learning目录下创建config-server子模块
```bash
$ mkdir -p config-server/src/{main,test}/{java,resources}
```

在spring-cloud-learning目录下的settings.gradle中引入config-server子模块
```gradle
include 'config-server'
```

在econfig-server添加build.gradle配置文件
```bash
touch config-server/build.gradle
```

修改config-server目录下的build.gradle配置文件
```gradle
archivesBaseName = 'config-server'

dependencies {
    compile('org.springframework.cloud:spring-cloud-config-server')
    compile('org.springframework.boot:spring-boot-starter-web')
    testCompile('org.springframework.boot:spring-boot-starter-test')
}
```