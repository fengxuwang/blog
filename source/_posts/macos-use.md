---
title: Mac OS使用常见问题
date: 2019-03-10 20:57:35
tags:
---
# 常见问题

## 终端新建远程连接内，如何清除连接记录历史记录
列出了你的所有历史记录
```bash
defaults read /Users/fengxuwang/Library/Preferences/com.apple.Terminal.plist PreviousCommands
```

运行下面的命令删除，然后重启终端
```bash
defaults delete /Users/fengxuwang/Library/Preferences/com.apple.Terminal.plist PreviousCommands
```