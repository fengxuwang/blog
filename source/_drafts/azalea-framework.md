---
title: azalea-framework
tags:
---

### Gradle工程架构

#### 创建父工程
```bash
$ mkdir azalea-project
```

#### 初始化gralde工程架构
```bash
$ cd azalea-project && gradle init
```

#### 创建子模块
```bash
$ mkdir -p azalea-eureka-server/src/{main,test}/{java,resources}
```

#### 修改父工程settings.gradle
```gradle
include 'azalea-eureka-server'
```

#### 修改父工程build.gradle
```gradle
plugins {
    id 'org.springframework.boot' version '2.1.3.RELEASE'
}

allprojects {
    group = "org.azaleaframework"
    version '0.0.1-SNAPSHOT'

    apply plugin: 'java'
    apply plugin: 'idea'
}

subprojects {
    apply plugin: 'org.springframework.boot'
    apply plugin: 'io.spring.dependency-management'

    sourceCompatibility = '1.8'
    targetCompatibility =  '1.8'

    repositories {
        maven {url 'http://maven.aliyun.com/nexus/content/groups/public/'}
        mavenCentral()
    }

    dependencyManagement {
        imports {
            mavenBom 'org.springframework.cloud:spring-cloud-dependencies:Finchley.SR2'
        }
    }
}
```

#### 子模块增加build.gradle配置文件
```bash
touch azalea-eureka-server/build.gradle
```

gradle 2.1版本及以上
```gradle
archivesBaseName = "azalea-eureka-server"

dependencies {
    compile('org.springframework.cloud:spring-cloud-starter-netflix-eureka-server')
    testCompile('org.springframework.boot:spring-boot-starter-test')

}
```

gradle 2.1以下
```gradle
buildscript {
    ext {
        springbootVersion='2.1.3.RELEASE'
    }
    
    repositories {
        maven {url 'http://maven.aliyun.com/nexus/content/groups/public/'}
        mavenCentral()
    }
    
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${springbootVersion}")
    }
}

apply plugin: 'java'
apply plugin: 'war'
apply plugin: 'idea'
apply plugin: 'org.springframework.boot'
apply plugin: 'io.spring.dependency-management'

repositories {
    maven {url 'http://maven.aliyun.com/nexus/content/groups/public/'}
    mavenCentral()
}

sourceCompatibility=1.8
targetCompatibility=1.8

dependencies {
    compile("org.springframework.boot:spring-boot-starter-web")
    testCompile("org.springframework.boot:spring-boot-starter-test")
}
```

#### 创建eureka-client
```bash
$ mkdir -p azalea-eureka-client/src/{main,test}/{java,resources}
```

添加build.gradle配置
```bash
touch azalea-eureka-client/build.gradle
```

修改build.gradle配置
```gradle
archivesBaseName = "azalea-eureka-client"

dependencies {
    compile('org.springframework.cloud:spring-cloud-starter-netflix-eureka-client')
    compile('org.springframework.boot:spring-boot-starter-web')
    testCompile('org.springframework.boot:spring-boot-starter-test')
}
```

修改settings.gradle
```gradle
include 'azalea-eureka-client'
```