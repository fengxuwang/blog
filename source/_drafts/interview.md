---
title: 常见问题
date: 2018-05-03 23:40:33
tags:
---

## Spring

### Spring事务传播行为
所谓事务的传播行为是指，如果在开始当前事务之前，一个事务上下文已经存在，此时有若干选项可以指定一个事务性方法的执行行为。在Propagation定义中包括了如下几个表示传播行为的常量：

- Propagation.REQUIRED：如果当前存在事务，则加入该事务；如果当前没有事务，则创建一个新的事务，这是默认值。
- Propagation.REQUIRES_NEW：创建一个新的事务，如果当前存在事务，则把当前事务挂起。
- Propagation.SUPPORTS：如果当前存在事务，则加入该事务；如果当前没有事务，则以非事务的方式继续运行。
- Propagation.NOT_SUPPORTED：以非事务方式运行，如果当前存在事务，则把当前事务挂起。
- Propagation.NEVER：以非事务方式运行，如果当前存在事务，则抛出异常。
- Propagation.MANDATORY：如果当前存在事务，则加入该事务；如果当前没有事务，则抛出异常。
- Propagation.NESTED：如果当前存在事务，则创建一个事务作为当前事务的嵌套事务来运行；如果当前没有事务，则该取值等价于TransactionDefinition.PROPAGATION_REQUIRED。

### Spring事务隔离级别
隔离级别是指若干个并发的事务之间的隔离程度。Isolation 接口中定义了五个表示隔离级别的常量：

- Isolation.DEFAULT：这是默认值，表示使用底层数据库的默认隔离级别。对大部分数据库而言，通常这值就是 Isolation.READ_COMMITTED。
- Isolation.READ_UNCOMMITTED：该隔离级别表示一个事务可以读取另一个事务修改但还没有提交的数据。该级别不能防止脏读，不可重复读和幻读，因此很少使用该隔离级别。比如PostgreSQL实际上并没有此级别。
- Isolation.READ_COMMITTED：该隔离级别表示一个事务只能读取另一个事务已经提交的数据。该级别可以防止脏读，这也是大多数情况下的推荐值。
- Isolation.REPEATABLE_READ：该隔离级别表示一个事务在整个过程中可以多次重复执行某个查询，并且每次返回的记录都相同。该级别可以防止脏读和不可重复读。
- Isolation.SERIALIZABLE：所有的事务依次逐个执行，这样事务之间就完全不可能产生干扰，也就是说，该级别可以防止脏读、不可重复读以及幻读。但是这将严重影响程序的性能。通常情况下也不会用到该级别。

### Spring和SpringMVC两个IOC容器有什么关系呢？

Spring的IOC容器包含了SpringMVC的IOC，即SpringMVC配置的bean可以调用Spring配置好的bean，反之则不可以。

如果SpringMVC想通过@Autowired注入Spring容器里的属性，即使Spring配置文件已经配置好了。

````
<context:component-scan base-package="com.wzy"></context:component-scan>
````

或者

````
<bean class="org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor"/>
````

SpringMVC配置文件中也得需要从新配置。
