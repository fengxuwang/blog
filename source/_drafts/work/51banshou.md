---
title: 金扳手工作笔记
date: 2018-05-30 17:26:54
tags:
---

### 51banshou-wx项目

#### 获取代码
```
$ git clone ssh://git@47.96.170.152:7999/ban/51banshou-wx.git
```

### 51banshou-infrastructure项目

#### 获取代码
```
$ git clone ssh://git@47.96.170.152:7999/ban/51banshou-infrastructure.git
```

### 51banshou-api项目

#### 获取代码
```
$ git clone --recursive ssh://git@47.96.170.152:7999/ban/51banshou-api.git
```

#### 项目目录结构说明

```lua
51banshou-api
├── src
|    ├── java
|           ├── com.banshou.api
|           ├── bo
|           ├── config
|           ├── controller -- 控制层
```

### 51banshou-oauth项目

#### 获取代码
```
$ git clone --recursive ssh://git@47.96.170.152:7999/ban/51banshou-oauth.git
```

### 51banshou-nts项目

#### 获取代码
```
$ git clone --recursive ssh://git@47.96.170.152:7999/ban/51banshou-nts.git
```

### 51banshou-management项目

#### 获取代码
```
$ git clone ssh://git@47.96.170.152:7999/ban/51banshou-management.git
```

### 51banshou-management-api项目

#### 获取代码
```
$ git clone --recursive ssh://git@47.96.170.152:7999/ban/51banshou-management-api.git
```




### 笔记

#### Android应用跳转到小程序

##### 添加Android开发工具包，在build.gradle配置文件中添加依赖
```gradle
dependencies {
    compile 'com.tencent.mm.opensdk:wechat-sdk-android-with-mta:+'
}
```

##### 代码示例
```java
private void openMiniProgram() {
    // 当前应用AppId
    String appId = "wxbf2283419156fda6"; 
    IWXAPI api = WXAPIFactory.createWXAPI(mContext, appId);

    WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
    // 小程序原始id
    req.userName = "gh_2fc11a4d003d";
    // 拉起小程序页面的可带参路径，不填默认拉起小程序首页
    req.path = "pages/index/index?stage=1&missionId=206&divisionId=2";  
    // 可选打开 开发版，体验版和正式版                
    req.miniprogramType = WXLaunchMiniProgram.Req.MINIPROGRAM_TYPE_TEST;
    api.sendReq(req);
}
```

##### 小程序接收APP传递的参数，在跳转到页面的js添加
```javascript
Page({
    onLoad: function(options) {
        // options包含App传递的参数
        // 格式： {stage:1,missionId:206,divisionId:2}
    }
});
```

##### 小程序跳转回APP

小程序打开APP，前提是你是从APP内打开小程序，才能返回打开APP，不然是不支持的。需要用户主动触发才能打开 APP

```javascript
<button open-type="launchApp" app-parameter="wechat" binderror="launchAppError">打开APP</button>

Page({ 
    launchAppError: function(e) { 
        console.log(e.detail.errMsg) 
    } 
})
```

##### APP接收小程序回调传回的参数
```java
public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
    @Override
    public void onReq(BaseReq req) {
    }

    @Override
    public void onResp(BaseResp resp) {
        if (resp.getType() == ConstantsAPI.COMMAND_LAUNCH_WX_MINIPROGRAM) {
            WXLaunchMiniProgram.Resp launchMiniProResp = (WXLaunchMiniProgram.Resp) resp;
            // 对应JsApi navigateBackApplication中的extraData字段数据
            String extraData = launchMiniProResp.extMsg; 
            Toast.makeText(this, extraData, Toast.LENGTH_LONG).show();
        }
    }
}
```

在AndroidManifest.xml中添加
```xml
<activity
    android:name=".wxapi.WXEntryActivity"
    android:label="@string/app_name"
    android:exported="true"
    android:launchMode="singleTop">
    <intent-filter>
        <action android:name="android.intent.action.MAIN" />
        <category android:name="android.intent.category.LAUNCHER" />
    </intent-filter>

    <intent-filter>
        <action android:name="android.intent.action.VIEW"/>
        <category android:name="android.intent.category.DEFAULT"/>
        <data android:scheme="sdksample"/>
    </intent-filter>
</activity>
```


dev:
数据库
ip：47.96.170.152
username：wdyk
password：Wdyk@2018

服务器
ip：47.97.120.146
username：root
password：Wdyk2013

mo:
数据库
ip：47.93.207.37
username：wdyk
password：Wdyk@123

服务器
ip：47.93.207.37
username：root
password：Wdyk2013

product:
数据库
ip：47.93.194.29
username：wdyk
password：Wdyk@123

服务器
ip：47.93.194.29
username：root
password：Wdyk2013